use argon2::verifier::{DecodeError, Encoded};
use argon2::{Argon2, Variant};
use rand::rngs::StdRng;
use rand::{FromEntropy, RngCore};

const SALT_LEN: usize = 32;

#[derive(Clone)]
pub struct Hasher {
    key: Vec<u8>,
    rng: StdRng,
}

impl Hasher {
    pub fn new(key: &[u8]) -> Hasher {
        Hasher {
            rng: StdRng::from_entropy(),
            key: key.to_owned(),
        }
    }

    pub fn hash(&mut self, password: &str) -> Vec<u8> {
        // Salt
        let mut salt = vec![0; SALT_LEN];
        self.rng.fill_bytes(&mut salt);
        let salt = salt;

        // Build Hash
        let argon = Self::new_argon();
        let enc = Encoded::new(argon, password.as_bytes(), &salt, &self.key, &[]);
        let ret = enc.to_u8();
        println!("generated hash {:X?}", ret);
        ret
    }

    pub fn verify(&self, password: &str, stored: &[u8]) -> Result<bool, DecodeError> {
        println!("retrieved hash {:X?}", stored);
        let enc = Encoded::from_u8(stored)?;

        Ok(enc.verify(password.as_bytes()))
    }

    pub fn gen_token(&mut self) -> Vec<u8> {
        let mut vec = vec![0; 32];
        self.rng.fill_bytes(&mut vec);

        vec
    }

    fn new_argon() -> Argon2 {
        Argon2::default(Variant::Argon2i)
    }
}
