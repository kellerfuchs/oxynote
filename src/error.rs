use std::result::Result as StdRes;

use actix::MailboxError;
use actix_web::http::header;
use actix_web::http::StatusCode as Status;
use actix_web::{self, HttpResponse, ResponseError};
use argon2::verifier::DecodeError;
use base64;
use chrono;
use diesel;
use r2d2;
use serde_json;

pub type Result<T, E = Error> = StdRes<T, E>;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "Database Error: {}", _0)]
    DbError(#[cause] diesel::result::Error),
    #[fail(display = "JSON (de)serialize error: {}", _0)]
    JsonErr(#[cause] serde_json::Error),
    #[fail(display = "Actix Error: {}", _0)]
    Actix(String),
    #[fail(display = "Database Pool Error: {}", _0)]
    R2d2(String),
    #[fail(display = "Unauthorized: {}", _0)]
    Unauthorized(String),
    #[fail(display = "Bad Client Data: {}", _0)]
    BadInput(&'static str),
    #[fail(display = "Malformed Header: {}", _0)]
    BadHeader(header::ToStrError),
    #[fail(display = "Internal Error: {}", _0)]
    Internal(&'static str),
}

impl Error {}

impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        let status = match self {
            Error::JsonErr(_)
            | Error::DbError(_)
            | Error::R2d2(_)
            | Error::Actix(_)
            | Error::Internal(_) => Status::INTERNAL_SERVER_ERROR,
            Error::Unauthorized(_) => Status::UNAUTHORIZED,
            Error::BadInput(_) | Error::BadHeader(_) => Status::UNPROCESSABLE_ENTITY,
        };

        let json = json!({
            "errors": [self.to_string()]
        });

        HttpResponse::build(status).json(json)
    }
}

impl From<serde_json::Error> for Error {
    fn from(error: serde_json::Error) -> Self {
        Error::JsonErr(error)
    }
}

impl From<MailboxError> for Error {
    fn from(error: MailboxError) -> Self {
        Error::Actix(error.to_string())
    }
}

impl From<actix_web::Error> for Error {
    fn from(error: actix_web::Error) -> Self {
        Error::Actix(error.to_string())
    }
}

impl From<diesel::r2d2::Error> for Error {
    fn from(inner: diesel::r2d2::Error) -> Self {
        Error::R2d2(inner.to_string())
    }
}

impl From<r2d2::Error> for Error {
    fn from(inner: r2d2::Error) -> Self {
        Error::R2d2(inner.to_string())
    }
}

impl From<diesel::result::Error> for Error {
    fn from(error: diesel::result::Error) -> Self {
        Error::DbError(error)
    }
}

impl From<header::ToStrError> for Error {
    fn from(error: header::ToStrError) -> Self {
        Error::BadHeader(error)
    }
}

impl From<chrono::ParseError> for Error {
    fn from(_: chrono::ParseError) -> Self {
        Error::BadInput("Malformed sync_token")
    }
}

impl From<DecodeError> for Error {
    fn from(e: DecodeError) -> Self {
        println!("argon2 error: {:#?}", e);
        Error::Internal("Password hash malformed")
    }
}

impl From<base64::DecodeError> for Error {
    fn from(_: base64::DecodeError) -> Self {
        Error::Internal("Password hash not base64")
    }
}
