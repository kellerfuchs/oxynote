use actix_web::middleware::session::RequestSession;
use actix_web::{FromRequest, HttpRequest, Result};

use uuid::Uuid;

use error::Error;
use model::Token;

use SfsApp;

impl FromRequest<SfsApp> for Token {
    type Result = Result<Token, Error>;
    type Config = ();

    fn from_request(req: &HttpRequest<SfsApp>, _: &Self::Config) -> Result<Token, Error> {
        use diesel::prelude::*;
        use model::schema::tokens::dsl::*;

        let sess = req.session();
        let state = req.state();

        let uid: Option<Uuid> = sess.get("user_id")?;
        let token: Option<String> = sess.get("token")?;

        if let (Some(uid), Some(token)) = (uid, token) {
            let conn = state.core.conn()?;

            let token: Token = tokens
                .filter(value.eq(token).and(user_id.eq(uid)))
                .first(&conn)
                .map_err(|_| Error::Unauthorized("Bad Cookie".into()))?;

            Ok(token)
        } else {
            Err(Error::Unauthorized("Authorization Missing".to_owned()))
        }
    }
}
