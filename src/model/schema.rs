table! {
    tokens (id) {
        id -> Uuid,
        client -> Text,
        value -> Text,
        created_at -> Timestamptz,
        last_used -> Timestamptz,
        user_id -> Uuid,
    }
}

table! {
    users (id) {
        id -> Uuid,
        username -> Text,
        password -> Bytea,
        email -> Text,
        verified -> Bool,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(tokens -> users (user_id));

allow_tables_to_appear_in_same_query!(
    tokens,
    users,
);
