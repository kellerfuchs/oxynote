use chrono::{DateTime, Utc};
use uuid::Uuid;

use model::schema::tokens;
use model::User;

#[derive(Debug, Clone, PartialEq, Queryable, Identifiable, Associations)]
#[table_name = "tokens"]
#[belongs_to(User, foreign_key = "user_id")]
pub struct Token {
    pub id: Uuid,
    pub client: String,
    pub value: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub user_id: Uuid,
}

#[derive(Debug, Deserialize, Insertable)]
#[table_name = "tokens"]
pub struct NewToken<'a> {
    pub client: &'a str,
    pub value: &'a str,
    pub user_id: Uuid,
}
