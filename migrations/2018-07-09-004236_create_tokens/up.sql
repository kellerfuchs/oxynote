-- Your SQL goes here
CREATE TABLE tokens (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    client TEXT NOT NULL,
    value TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    last_used TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    user_id UUID REFERENCES users(id) NOT NULL
);